# Ta datoteka, je glavna datoteka za naso igrico, kar tudi ime pove.
# Knjiznice
import random
from tkinter import * 
# Globalne spremenljivke
X = 10 #koordinatni sistem je zavrten za 90°
Y = 10
POLJE_X = 30*X + 2
POLJE_Y = 30*Y + 2
LADJE = [5,4,3,3,2]
IMENA = {i:chr(65 + i) for i in range(0, Y)}

# objekt ciljanje, v katerem je zapisana logika za računalnikovo ciljanje
class Ciljanje():
    
    def __init__(self):
        pass
    
    # najbolj neumna metoda ciljanja, ki kar nekam strelja,
    # ne more pa dvakrat v isto polje
    def easy(self, polje):
        ze_zadeto = True
        while ze_zadeto:
            (i, j) = [random.randint(0,9), random.randint(0,9)]
            if polje[i][j] == 0:
                ze_zadeto = False
        return [i, j]
    # metoda cilja nakljucno, ampak ko zadane ladjo
    # jo skuša potopiti, kot bi jo skušal potopiti "človek"
    def normal(self, polje, prejsnji, ladje):  
        # če ladje se nismo potopili, ali nismo še nič zadeli
        # nakljucno ciljamo
        if prejsnji == None:
            ze_zadeto = True
            while ze_zadeto:
                (i, j) = [random.randint(0,9), random.randint(0,9)]
                if polje[i][j] == 0:
                    ze_zadeto = False
            return [i, j]
        # sicer pa malo razmislimo, kam gre ciljati
        else:
            return self.potopi(polje, prejsnji, ladje)
            
    # metoda cilja naklucno, s tem da ve, da ladje ne morajo biti
    # postavljene skupaj              
    def hard(self, polje, prejsnji, ladje):
        # če ladje se nismo potopili, ali nismo še nič zadeli
        # nakljucno ciljamo, s tem, da pazimo, kje so ladje v blizini
        if prejsnji == None:
            veljaven = True
            while veljaven:
                (i, j) = [random.randint(0,9), random.randint(0,9)]
                if polje[i][j] == 0:
                    veljaven = False
                if i - 1 >= 0:
                    if polje[i-1][j] == 1:
                        veljaven = True
                if i + 1 <= Y - 1:
                    if polje[i+1][j] == 1:
                        veljaven = True
                if j - 1 >= 0:
                    if polje[i][j-1] == 1:
                        veljaven = True
                if j + 1 <= X - 1:
                    if polje[i][j+1] == 1:
                        veljaven = True
            return [i, j]
        # sicer pa malo razmislimo, kam gre ciljati
        else:
            return self.potopi(polje, prejsnji, ladje)
    def impossible(self, polje, prejsnji, ladje, zgreseni, streli):
        if streli > 8:
            na_robu = 3
        else:
            na_robu = 8
        if prejsnji == None:
            if zgreseni > 2 and zgreseni % 2 == 0:
                return self.ciljaj_rob(polje, ladje)
            else:
                return self.isci_ladje(polje, ladje)
        else:
            return self.potopi(polje, prejsnji, ladje, True)
            
    # ce igralec postavi vse ladje ob rob,
    # racunalnik poskusi ciljati tja            
    def ciljaj_rob(self, polje, ladje):
        potencialni = list()
        dolzine = list()
        for ladja in ladje:
            if not ladja.potopljena:
                dolzine.append(ladja.dolzina)
        
        robovi = ["gor", "dol", "levo", "desno"]
        for rob in robovi:
            for dolzina in dolzine:
                if rob == "gor":
                    tocke = [ 0 for j in range(X)]
                    x = 0
                    for j in range(Y):      
                        if polje[x][j] in [-1, 1]:
                            tocke[j] = -10
                            continue
                        else:
                            for k in range(-1, dolzina + 1):
                                for l in range(dolzina):
                                    ok = True
                                    indeks = j - dolzina + k + l
                                    if indeks >= 0 and indeks <= Y - 1:
                                        if (k == -1 and l == 0) or (k == dolzina and l == dolzina - 1):
                                            if polje[x][indeks] == 1:
                                                ok = False
                                        if polje[x][indeks] in [1, -1]:
                                            ok = False
                                        if x-1 >= 0:
                                            if polje[x-1][indeks] == 1:
                                                ok = False
                                                tocke[indeks] = -14
                                        if x+1 <= Y - 1:
                                            if polje[x+1][indeks] == 1:
                                                ok = False
                                                tocke[indeks] = -14
                                    else:
                                        ok = False
                                if ok:
                                    tocke[j] += 1
                    najvec = max(tocke)
                    for j in range(Y):
                        if tocke[j] == najvec:
                            potencialni.append([x, j])
                elif rob == "dol":
                    tocke = [ 0 for j in range(X)]
                    x = Y - 1
                    for j in range(Y):      
                        if polje[x][j] in [-1, 1]:
                            tocke[j] = -10
                            continue
                        else:
                            for k in range(-1, dolzina + 1):
                                for l in range(dolzina):
                                    ok = True
                                    indeks = j - dolzina + k + l
                                    if indeks >= 0 and indeks <= Y - 1:
                                        if (k == -1 and l == 0) or (k == dolzina and l == dolzina - 1):
                                            if polje[x][indeks] == 1:
                                                ok = False
                                        if polje[x][indeks] in [1, -1]:
                                            ok = False
                                        if x-1 >= 0:
                                            if polje[x-1][indeks] == 1:
                                                ok = False
                                                tocke[indeks] = -14
                                        if x+1 <= Y - 1:
                                            if polje[x+1][indeks] == 1:
                                                ok = False
                                                tocke[indeks] = -14
                                    else:
                                        ok = False
                                if ok:
                                    tocke[j] += 1
                    najvec = max(tocke)
                    for j in range(Y):
                        if tocke[j] == najvec:
                            potencialni.append([x, j])
                elif rob == "levo":
                    tocke = [0 for i in range(Y)]
                    y = 0
                    for i in range(X):         
                        if polje[i][y] in [-1, 1]:
                            tocke[i] = -10
                            continue
                        else:
                            for k in range(-1, dolzina + 1):
                                for l in range(dolzina):
                                    ok = True
                                    indeks = i - dolzina + k + l
                                    if indeks >= 0 and indeks <= X - 1:
                                        if (k == -1 and l == 0) or (k == dolzina and l == dolzina - 1):
                                            if polje[indeks][y] == 1:
                                                ok = False
                                        if polje[indeks][y] in [1, -1]:
                                            ok = False
                                        if y-1 >= 0:
                                            if polje[indeks][y-1] == 1:
                                                ok = False
                                                tocke[indeks] = -14
                                        if y+1 <= X - 1:
                                            if polje[indeks][y+1] == 1:
                                                ok = False
                                                tocke[indeks] = -14
                                    else:
                                        ok = False
                                if ok:
                                    tocke[i] += 1
                    # poiscemo vse tocke, ki imajo element najvec
                    najvec = max(tocke)
                    for i in range(X):
                        if tocke[i] == najvec:
                            potencialni.append([i, y])
                else:
                    tocke = [0 for i in range(Y)]
                    y =  X - 1 
                    for i in range(X):         
                        if polje[i][y] in [-1, 1]:
                            tocke[i] = -10
                            continue
                        else:
                            for k in range(-1, dolzina + 1):
                                for l in range(dolzina):
                                    ok = True
                                    indeks = i - dolzina + k + l
                                    if indeks >= 0 and indeks <= X - 1:
                                        if (k == -1 and l == 0) or (k == dolzina and l == dolzina - 1):
                                            if polje[indeks][y] == 1:
                                                ok = False
                                        if polje[indeks][y] in [1, -1]:
                                            ok = False
                                        if y-1 >= 0:
                                            if polje[indeks][y-1] == 1:
                                                ok = False
                                                tocke[indeks] = -14
                                        if y+1 <= X - 1:
                                            if polje[indeks][y+1] == 1:
                                                ok = False
                                                tocke[indeks] = -14
                                    else:
                                        ok = False
                                if ok:
                                    tocke[i] += 1
                    # poiscemo vse tocke, ki imajo element najvec
                    najvec = max(tocke)
                    for i in range(X):
                        if tocke[i] == najvec:
                            potencialni.append([i, y])
        return potencialni[random.randint(0, len(potencialni) - 1)]
        
    def potopi(self, polje, prejsnji, ladje, pamet = False):
        delaj = True
        dolzine = list()
        potencialni = list()
        okolica = [[None, None], [None, None]]
        (i, j) = prejsnji
        # sklepamo, da iscemo najdaljso nepotopljeno ladjo
        for ladja in ladje:
            if not ladja.potopljena:
                dolzina = ladja.dolzina
                break
        for ship in ladje:
            if not ladja.potopljena:
                dolzine.append(ship.dolzina)
        #najprej pogledamo okolico in si jo zapomnemo
        if i-1 >= 0:
            okolica[0][0] =  polje[i-1][j]
        if i+1 <= Y-1:
            okolica[0][1] = polje[i+1][j]
        if j-1 >= 0:
            okolica[1][0] =  polje[i][j-1]
        if j+1 <= X-1:
            okolica[1][1] = polje[i][j+1]
        # nad in pod je vsaj eno zadeto polje
        if 1 in okolica[0]:
            delaj = False
            #polje nad je zadeto, polje spodaj je zgrešeno
            if okolica[0][0] == 1 and okolica[0][1] == -1:
                for k in range(dolzina):
                    if i-k >= 0:
                        if polje[i-k][j] == 0:
                            potencialni.append([i-k, j])
                            break
            #polje nad je zadeto, polje spodaj je prazno
            elif okolica[0][0] == 1 and okolica[0][1] in [0, None]:
                if i+1 <= Y - 1:
                    potencialni.append([i+1, j])
                for k in range(dolzina):
                    if i-k >= 0:
                        if polje[i-k][j] == 0:
                            potencialni.append([i-k, j])
                            break
                        elif polje[i-k][j] == -1:
                            break
            #polje spodaj je zadeto, polje nad je zgrešeno
            if okolica[0][1] == 1 and okolica[0][0] == -1:
                for k in range(dolzina):
                    if i+k <=  Y - 1:
                        if polje[i+k][j] == 0:
                            potencialni.append([i+k, j])
                            break
            #polje spodaj je zadeto, polje nad je prazno
            elif okolica[0][1] == 1 and okolica[0][0] in [0, None]:
                if i-1 >= 0:
                    potencialni.append([i-1, j])
                for k in range(dolzina ):
                    if i+k <= Y - 1:
                        if polje[i+k][j] == 0:
                            potencialni.append([i+k, j])
                            break
                        if polje[i+k][j] == -1:
                            break
                    
                
        # levo ali desno je vsaj eno zadeto polje
        elif 1 in okolica[1]:
            delaj = False
            #polje levo je zadeto, polje desno je zgrešeno
            if okolica[1][0] == 1 and okolica[1][1] == -1:
                for k in range(dolzina):
                    if j-k >= 0:
                        if polje[i][j-k] == 0:
                            potencialni.append([i, j-k])
                            break
            #polje levo je zadeto, polje desno je prazno
            elif okolica[1][0] == 1 and okolica[1][1] in [0, None]:
                if j+1 <= X - 1:
                    potencialni.append([i, j+1])
                for k in range(dolzina):
                    if j-k >= 0:
                        if polje[i][j-k] == 0:
                            potencialni.append([i, j-k])
                            break
                        elif polje[i][j-k] == -1:
                            break
            #polje desno je zadeto, polje levo je zgrešeno
            if okolica[1][1] == 1 and okolica[1][0] == -1:
                for k in range(dolzina):
                    if j+k <=  X - 1:
                        if polje[i][j+k] == 0:
                            potencialni.append([i, j+k])
                            break
            #polje desno je zadeto, polje levo je prazno
            elif okolica[1][1] == 1 and okolica[1][0] in [0, None]:
                if j-1 >= 0:
                    potencialni.append([i, j-1])
                for k in range(dolzina):
                    if j+k <=  X - 1:
                        if polje[i][j+k] == 0:
                            potencialni.append([i, j + k])
                            break
                        elif polje[i][j+k] == -1:
                            break
        if not pamet:
             #v okolici je vsaj eno zgrešeno polje
            if -1 in okolica[0] or -1 in okolica[1]:
                for k in range(2):
                    for l in range(2):
                        if okolica[k][l] not in [None, -1]:
                            if k == 0:
                                potencialni.append([i -1, j] if l == 0 else [i + 1, j])
                            else:
                                potencialni.append([i, j-1] if l == 0 else [i, j + 1])
        # okoli so sama prazna polja
            else:
                for k in range(2):
                    for l in range(2):
                        if okolica[k][l] != None:
                            if k == 0:
                                potencialni.append([i -1, j] if l == 0 else [i + 1, j])
                            else:
                                potencialni.append([i, j-1] if l == 0 else [i, j + 1])
        # ta je malo pametnejša, in gleda, v katero smer je ladja sploh lahko postavljena
        else:
            if delaj:
                stej_ladje = 0
                while len(potencialni) == 0:
                    dolzina = dolzine[stej_ladje]
                    if dolzina == 5:
                        poglej = 3
                    else:
                        poglej = 2
                    for k in range(4):
                        ok = True
                        if k == 0:
                            for l in range(1,poglej):
                                if i+l <= Y - 1:
                                    if polje[i+l][j] in [1, -1]:
                                        ok = False
                                else:
                                    ok = False
                            try:
                                if polje[i + dolzina][j] == 1:
                                    ok = False
                            except IndexError:
                                pass
                            if ok:
                                    potencialni.append([i+1, j])
                        elif k == 1:
                            for l in range(1,dolzina):
                                if i-l >= 0:
                                    if polje[i-l][j] in [1, -1]:
                                        ok = False
                                else:
                                    ok = False
                            try:
                                if polje[i - dolzina][j] == 1:
                                    ok = False
                            except IndexError:
                                pass
                            if ok:
                                potencialni.append([i-1, j])
                        elif k == 2:
                            for l in range(1,dolzina):
                                if j+l <= X - 1:
                                    if polje[i][j + l] in [1, -1]:
                                        ok = False
                                else:
                                    ok = False
                            try:
                                if polje[i][j + dolzina] == 1:
                                    ok = False
                            except IndexError:
                                pass
                            if ok:
                                potencialni.append([i, j + 1])
                        else:
                            for l in range(1,dolzina):
                                if j - l >= 0:
                                    if polje[i][j - l] in [1, -1]:
                                        ok = False
                                else:
                                    ok = False
                            try:
                                if polje[i][j - dolzina] == 1:
                                    ok = False
                            except IndexError:
                                pass
                            if ok:
                                potencialni.append([i, j - 1])
                    stej_ladje += 1
                    
        # vrne eno od polj za naslednji strel.
        return potencialni[random.randint(0, len(potencialni)-1)]
    # metoda poisce najbolj verjetne tocke, kjer bi se ladja lahko
    # nahajala. Vedno išče najdaljšo ladjo.
    def isci_ladje(self, polje, ladje):
        potencialni = list()
        #tukaj notri shranimo dolžine nepotopljenih ladij
        dolzine = list()
        tocke = [[0 for i in range(Y)] for j in range(X)]
        for ladja in ladje:
            if not ladja.potopljena:
                dolzine.append(ladja.dolzina)
        for dolzina in dolzine:
            for smer in ["hor", "ver"]:
                for i in range(Y):
                    for j in range(X):
                        if smer == "hor":
                            if polje[i][j] in [-1, 1]:
                                tocke[i][j] = -10
                                continue
                            else:
                                for k in range(-1, dolzina + 1):
                                    for l in range(dolzina):
                                        ok = True
                                        indeks = j - dolzina + k + l
                                        if indeks >= 0 and indeks <= Y - 1:
                                            if (k == -1 and l == 0) or (k == dolzina and l == dolzina - 1):
                                                if polje[i][indeks] == 1:
                                                    ok = False
                                            if polje[i][indeks] in [1, -1]:
                                                ok = False
                                            if i-1 >= 0:
                                                if polje[i-1][indeks] == 1:
                                                    ok = False
                                                    tocke[i-1][indeks] = -14
                                            if i+1 <= Y - 1:
                                                if polje[i+1][indeks] == 1:
                                                    ok = False
                                                    tocke[i+1][indeks] = -14
                                        else:
                                            ok = False
                                    if ok:
                                        tocke[i][j] += 1
                        else:
                            if polje[i][j] in [-1, 1]:
                                tocke[i][j] = -10
                                continue
                            else:
                                for k in range(-1, dolzina + 1):
                                    for l in range(dolzina):
                                        ok = True
                                        indeks = i - dolzina + k + l
                                        if indeks >= 0 and indeks <= X - 1:
                                            if (k == -1 and l == 0) or (k == dolzina and l == dolzina - 1):
                                                if polje[indeks][j] == 1:
                                                    ok = False
                                            if polje[indeks][j] in [1, -1]:
                                                ok = False
                                            if j-1 >= 0:
                                                if polje[indeks][j-1] == 1:
                                                    ok = False
                                                    tocke[indeks][j-1] = -14
                                            if j+1 <= X - 1:
                                                if polje[indeks][j+1] == 1:
                                                    ok = False
                                                    tocke[indeks][j+1] = -14
                                        else:
                                            ok = False
                                    if ok:
                                        tocke[i][j] += 1

#         for vrstica in tocke:
#             print(vrstica)
#         print ("\n")
        # poiscemo najvecji element
        najvec = -10
        for vrstica in tocke:
            if max(vrstica) > najvec:
                najvec = max(vrstica)
        # poiscemo vse tocke, ki imajo element najvec
        for i in range(Y):
            for j in range(X):
                if tocke[i][j] == najvec:
                    potencialni.append([i, j])
                    
        #for vrstica in potencialni:
        #   print("{}, {}".format(IMENA[j], i))
        return potencialni[random.randint(0, len(potencialni)-1)]
                                    
                                    
                
                
            

#===============================================================================
# # objekt ladja
#===============================================================================
class Ladja():
    
    def __init__(self, dolzina):
        self.potopljena = False
        self.postavljena = False
        self.dolzina = dolzina
        self.rotacija = "ver"
        self.x = None
        self.y = None
        
    def __str__(self):
        return ("Ladja dolzina = {0}, potopljena = {1}".format(self.dolzina, self.potopljena))
    
    # metoda, ki preveri ali je ladja potopljena
    def je_potopljena(self, polje):
        potopljena = True
        for k in range(self.dolzina):
            if self.rotacija == "hor":
                if polje[self.y][self.x +k] != 1:
                    potopljena = False
            else:
                if polje[self.y + k][self.x] != 1:
                    potopljena = False     
        self.potopljena = potopljena
        return potopljena
    def pobarvaj_potopljene(self, canvas, kvadratki):
        if self.potopljena:
            for k in range(self.dolzina):
                if self.rotacija == "hor":
                    canvas.itemconfig(kvadratki[self.y][self.x+k], fill = "dark khaki")
                else:
                    canvas.itemconfig(kvadratki[self.y+k][self.x], fill = "dark khaki")
                    
                
            
        
#===============================================================================
# objekt igralec 
#===============================================================================
class Igralec():
    
    def __init__(self, je_clovek, id, tezavnost):
        self.je_clovek = je_clovek
        if not self.je_clovek:
            self.tezavnost = tezavnost
        # prazni polji za ciljanje in postavljanje ladij
        self.moje_polje = [[0 for i in range(Y)]for j in range(X)]
        self.nasp_polje = [[0 for i in range(Y)] for j in range(X)]
        #tabela, ki vsebuje objetke Ladja
        self.ladje = [Ladja(LADJE[i]) for i in range(len(LADJE))]
        self.id = id
        # katere ladje so ze potopljene
        self.potopljene = [False for i in range(len(LADJE))]
        #števec za strele
        self.streli = 0
        #števec za zadetke
        self.zadetki = 0
        # zapomni si prejšnji zadetek
        self.prejsnji = None
        # steje kolikokrat je zgresil
        # to pomeni, da so vse ladje verjetno na robu
        self.zgreseni = 0
        


    
    def __repr__(self):
        return "Igralec({0})".format(self.je_clovek)
    
    def __str__(self):
        return "Igralec" if self.je_clovek else "Racunalnik"
#===============================================================================
# Glavni objekt, kjer se dogaja večino stvari glede igre
#===============================================================================
class Game():
    
    #init naredi igrico in oba igralca
    def __init__(self, master, tezavnost0, tezavnost1, igralec, racunalnik):
        self.igralec = Igralec(igralec, 0, tezavnost0)
        self.racunalnik = Igralec(racunalnik, 1, tezavnost1)
        # objekt, ki vsebuje vso inteligenco 
        self.ciljanje = Ciljanje()
        # kdo je trenutno na vrsti
        self.na_potezi = 0 # 0 za igralec, 1 za racunalnik     
        # spremenljivki, ki se pomikata po polju, takrat ko postavljamo ladje
        self.vrstica = random.randint(0, Y - 6)
        self.stolpec = random.randint(0, X - 1)
        # katero ladjo po vrsti postavljamo
        self.ladja = 0
        # ali se je igra že začela, ali še izbiramo
        self.zacetek_igre = False
        # ali je igre ze konec
        self.konec = False
        # naredimo vsa polja ta risanje
        self.canvas_levi = Canvas(master, width=POLJE_X, height=POLJE_Y, highlightthickness= 0)
        self.canvas_desni = Canvas(master, width=POLJE_X, height=POLJE_Y)
        self.canvas_bot = [Canvas(master, width=POLJE_X, height=Y+5), Canvas(master, width=POLJE_X, height=Y+5)]
        self.canvas_mid = Canvas(master, width=X+7, height=POLJE_Y)
        self.okno = master
        self.gumb_nova = Button(master, text="nova igra", command=self.nova)
        # kvadratki ki bodo prikazovali ladjo, ki jo bomo na začetku izbirali
        # napisi za polja
        self.crke = [chr(i) for i in range(65, 65 + Y)]
        self.stevilke = [str(i) for i in range(1, 1 + X)]
        self.navodila = StringVar(value = "Premikanje: smerne tipke, rotiraj: 'r', postavi: 'presledek', zamenjaj ladjo: 't'.")
        self.info = StringVar(value = "Postavi vse ladje.")
        self.label_navodila = Label(master, textvariable = self.navodila)
        self.label_info = Label(master, textvariable = self.info)
        self.ime_moje_polje = StringVar(value = "Moje polje")
        self.ime_nasp_polje= StringVar(value = "Nasprotnikovo polje")
        self.label_ime_moje_polje = Label(master, textvariable = self.ime_moje_polje)
        self.label_ime_nasp_polje = Label(master, textvariable = self.ime_nasp_polje)
         # povezemo polji z eventi
        if self.igralec.je_clovek:
            self.canvas_desni.bind("<Button-1>", self.strel)
            self.canvas_levi.bind("<Up>", self.prem_gor)
            self.canvas_levi.bind("<Left>", self.prem_levo)
            self.canvas_levi.bind("<Right>", self.prem_desno)
            self.canvas_levi.bind("<Down>", self.prem_dol)
            self.canvas_levi.bind("<space>", self.potrdi)
            self.canvas_levi.bind("t", self.zamenjaj)
            self.canvas_levi.bind("r", self.rotiraj)
        if not self.igralec.je_clovek:
            #gumb, ki bo zacel dvoboj med dvema racunalnikoma
            self.gumb_start = Button(master, text ="zacni", command = self.racvsrac)
        #poklicemo metodo za risanje zacetnega polja
        self.narisi()
        if self.igralec.je_clovek:
            self.pokazi_ladjo()
       
        #preverimo, ce se igra lahko začne
        #to je v primeru, ko je računalnik proti računalniku
        self.zacni_igro()
    
        
        
    #===========================================================================
    # #naredi podrocje za risanje
    #===========================================================================
    def narisi(self):
        self.okno.columnconfigure(0, minsize = 399)
        self.okno.columnconfigure(2, minsize = 399)
        self.label_navodila.grid(row = 0, column = 0)
        self.label_info.grid(row = 0, column = 2)
        self.canvas_levi.grid(row = 1, column = 0)
        self.canvas_desni.grid(row = 1, column = 2)
        self.canvas_mid.grid(row = 1, column = 1)
        self.canvas_bot[0].grid(row = 2, column = 0)
        self.canvas_bot[1].grid(row = 2, column = 2)
        self.label_ime_moje_polje.grid(row = 3, column = 0)
        self.label_ime_nasp_polje.grid(row = 3, column = 2)
        self.gumb_nova.grid(row = 4, column = 0)
        if not self.igralec.je_clovek:
            self.gumb_start.grid(row = 4, column = 2)
        
        # risanje napisov za mrezo
        x = 3 +  X // 2
        y = 3 * Y // 2 +1
        korak = 30
        for i in range(X):
            self.canvas_mid.create_text(x,y, text = self.stevilke[i])
            y += korak
        y = 2 + Y // 2
        x = 3 * X // 2 + 2
        for i in range(Y):
            self.canvas_bot[0].create_text(x,y, text = self.crke[i])
            self.canvas_bot[1].create_text(x,y, text = self.crke[i])
            x += korak
        #narisi vse kvadratke za mrezo in si jih zapomni
        self.moje_kvad =  [[0 for i in range(Y)]for j in range(X)]
        self.nasp_kvad =  [[0 for i in range(Y)]for j in range(X)]
        x = 2
        y = 2
        korak = 30
        for vrstica in range(X):
            for stolpec in range(Y):
                if self.igralec.moje_polje[vrstica][stolpec] == 0:
                    self.moje_kvad[vrstica][stolpec] = self.canvas_levi.create_rectangle(x,y,x+korak,y+korak, fill = "white")
                else:
                    self.moje_kvad[vrstica][stolpec] = self.canvas_levi.create_rectangle(x,y,x+korak,y+korak, fill = "blue")
                self.nasp_kvad[vrstica][stolpec] = self.canvas_desni.create_rectangle(x,y,x+korak,y+korak, fill = "white", activefill = "red")
                x +=korak
            x = 2
            y +=korak
        if not self.zacetek_igre and self.igralec.je_clovek:
            # te kvadratki se potem postavljajo tam, kamor hocemo dati ladjo
            self.kvadratki = [self.canvas_levi.create_rectangle(2,2,32,32, fill = "magenta")
                              for i in range(max(LADJE))]
    # prestavi ladjo gor, kadar pritisnemo
    # tipko gor   
    def prem_gor(self, event):
        if not self.zacetek_igre:
            if self.igralec.je_clovek:
                if self.vrstica > 0:
                    self.vrstica -= 1
                self.pokazi_ladjo()
                    
     # prestavi ladjo levo, kadar pritisnemo
    # tipko levo           
    def prem_levo(self, event):
        if not self.zacetek_igre:
            if self.igralec.je_clovek:
                if self.stolpec > 0:
                    self.stolpec -= 1
                self.pokazi_ladjo()
    # prestavi ladjo desno, kadar pritisnemo
    # tipko desno
    def prem_desno(self, event):
        if not self.zacetek_igre:
            if self.igralec.je_clovek:
                dolzina = self.igralec.ladje[self.ladja].dolzina
                postavljena = self.igralec.ladje[self.ladja].postavljena
                rotacija = self.igralec.ladje[self.ladja].rotacija
                if rotacija == "hor":
                    if self.stolpec + dolzina < X:
                        self.stolpec += 1
                else:
                    if self.stolpec < X - 1:
                        self.stolpec += 1
                self.pokazi_ladjo()
    # prestavi ladjo dol, kadar pritisnemo
    # tipko dol
    def prem_dol(self, event):
        if not self.zacetek_igre:
            if self.igralec.je_clovek:
                dolzina = self.igralec.ladje[self.ladja].dolzina
                postavljena = self.igralec.ladje[self.ladja].postavljena
                rotacija = self.igralec.ladje[self.ladja].rotacija
                if rotacija == "ver":
                    if self.vrstica + dolzina < Y:
                        self.vrstica += 1
                else:
                    if self.vrstica < Y -1:
                        self.vrstica += 1
                self.pokazi_ladjo()
    # obrne ladjo, kadar pritisnemo presledek
    def rotiraj(self, event):
        if not self.zacetek_igre:
            postavljena = self.igralec.ladje[self.ladja].postavljena
            rotacija = self.igralec.ladje[self.ladja].rotacija
            if rotacija == "ver":
                self.igralec.ladje[self.ladja].rotacija = "hor"
            else:
                self.igralec.ladje[self.ladja].rotacija = "ver"
            self.popravi_postavitev()
            self.pokazi_ladjo()
    # ladjo, ki sega čez rob postavi nazaj na igralno polje    
    def popravi_postavitev(self):
        dolzina = self.igralec.ladje[self.ladja].dolzina
        postavljena = self.igralec.ladje[self.ladja].postavljena
        rotacija = self.igralec.ladje[self.ladja].rotacija
        # ko narisemo novo ladjo, mora biti cela na igralnem polju
        if rotacija == "ver":
            if self.vrstica + dolzina > Y:
                self.vrstica = Y - dolzina
        else:
            if self.stolpec + dolzina > Y:
                self.stolpec = X - dolzina
        
        
    # nakljucno postavimo ladje
    def postavi_rac(self, id):
        if id == 0:
            ladje = self.igralec.ladje
            polje = self.igralec.moje_polje
        else:
            ladje = self.racunalnik.ladje
            polje = self.racunalnik.moje_polje
        # ta stevec se poveca, ko postavimo eno ladjo
        stevilka = 0
        # dokler niso vse ladje postavljene
        while stevilka < len(LADJE):
            a = random.randint(0,1)
            dolzina = LADJE[stevilka]
            rotacija = "hor" if a == 0 else "ver"
            ladje[stevilka].rotacija = rotacija
            if rotacija == "hor":
                x = random.randint(0,9)
                y = random.randint(0,9-dolzina)
            else:
                x = random.randint(0,9-dolzina)
                y = random.randint(0,9)
            #print("x = {}, y = {}, rotacija = {}, stevilka = {}".format(x,y,rotacija, stevilka))
            # najprej preverimo, če je ladja pravilno postavljena
            # če je, jo zapišemo tja
            if self.preveri_postavitev(id, stev = stevilka, i = x, j = y, rotacija = rotacija):
                (ladje[stevilka].x, ladje[stevilka].y) = (y,x)
                for k in range(dolzina):
                    if rotacija == "hor":
                        polje[x][y + k] = 1
                    else:
                        polje[x + k][y] = 1
                if id == 0:
                    self.igralec.ladje[stevilka].postavljena = True
                else:
                    self.racunalnik.ladje[stevilka].postavljena = True
                stevilka += 1
        return polje
                

            
            
            
            
    # metoda preveri, če je možno ladjo postaviti na mesto
    # kjer je trenutno, to dela tudi za računalniškega igralca
    # kar bo koristno, če bo ta igrica kdaj za
    # računalnik proti računalniku   
    def preveri_postavitev(self, id, i = None, j = None, stev = None, rotacija = None):
        if id == 0:
            if self.igralec.je_clovek:
                i = self.vrstica
                j = self.stolpec
                stev = self.ladja
                dolzina = self.igralec.ladje[self.ladja].dolzina
                postavljena = self.igralec.ladje[self.ladja].postavljena
                rotacija = self.igralec.ladje[self.ladja].rotacija
                polje = self.igralec.moje_polje
            else:
                postavljena = False
                dolzina = self.igralec.ladje[stev].dolzina
                postavljena = self.igralec.ladje[stev].postavljena
                rotacija = self.igralec.ladje[stev].rotacija
                polje = self.igralec.moje_polje
                #print("i = {}, j = {}, dolzina = {}, rotacija = {}".format(i,j,dolzina,rotacija))
        else:
            postavljena = False
            dolzina = self.racunalnik.ladje[stev].dolzina
            postavljena = self.racunalnik.ladje[stev].postavljena
            rotacija = self.racunalnik.ladje[stev].rotacija
            polje = self.racunalnik.moje_polje
        v_redu = True
        for k in range(dolzina):
            #splošen primer
            #preveri ce je poleg ali tu kakšna ladja ze postavljena
            for delta in range(-1,2):
                try:
                    if rotacija == "hor":
                        if i + delta >= 0:
                            if polje[i+delta][j+k] == 1:
                                v_redu = False            
                    else:
                        if j + delta >= 0:
                            if polje[i+k][j+delta] == 1:
                                v_redu = False 
                except IndexError:
                    pass                 
        try:
            if rotacija == "hor":
                if j-1 >= 0:
                    if polje[i][j-1] == 1:
                        v_redu = False
                if polje[i][j+dolzina] == 1:
                    v_redu = False
            else:
                if i - 1 >= 0:
                    if polje[i-1][j] == 1:
                        v_redu = False
                        #print("i = {}, j = {}".format(i+delta, j+k))
                if polje[i+dolzina][j] == 1:
                    v_redu = False  
        except IndexError:
            pass
        return v_redu     
    # v igralcevo matriko napise enice na ustrezna
    # mesta, kamor je postavil ladjo, če je to le mogoče   
    def potrdi(self, event):
        if not self.zacetek_igre:
            dolzina = self.igralec.ladje[self.ladja].dolzina
            postavljena = self.igralec.ladje[self.ladja].postavljena
            rotacija = self.igralec.ladje[self.ladja].rotacija
            # najprej preverimo, ali lahko ladjo sploh postavimo tja.
            if not postavljena:
                # preverimo, ce je postavitev v redu in ce je
                # ladjo lahko postavimo.
                # ustrezna polja nastavimo na 1
                if self.preveri_postavitev(self.igralec.id):
                    (self.igralec.ladje[self.ladja].x, self.igralec.ladje[self.ladja].y) = (self.stolpec,self.vrstica)
                    for i in range(dolzina):
                        if rotacija == "hor":
                            self.igralec.moje_polje[self.vrstica][self.stolpec + i] = 1
                        else:
                            self.igralec.moje_polje[self.vrstica + i][self.stolpec] = 1
            
                    self.igralec.ladje[self.ladja].postavljena = True
                    self.zamenjaj()
                    self.narisi()
                    self.zacni_igro(self.preveri())
            self.pokazi_ladjo()
                        
    # v konzolo izpiše matiko polja, da vidimo, kaj se dogaja.
    def printaj(self, polje):
        for vrstica in polje:
            print(vrstica)
        print("\n")
        
    # preveri, ce so vse ladje ze postavljene in zacne igro.
    def preveri(self):
        vse_postavljene = True
        for ladja in range(len(LADJE)):
            if not self.igralec.ladje[ladja].postavljena:
                vse_postavljene = False
        return vse_postavljene
    # omogoca izbiro med ladjami, dokler vseh ne postavimo
    # treba bo narediti eno while zanko, ki bo preverjala, če so ladje že postavljene
    def zamenjaj(self, event = None):
        if not self.zacetek_igre:
            if self.igralec.je_clovek:
                cez_vse = 0
                while cez_vse < len(LADJE):
                    if self.ladja < len(LADJE) - 1:
                        self.ladja += 1
                        if not self.igralec.ladje[self.ladja].postavljena:
                            break
                    else:
                        self.ladja = 0
                        if not self.igralec.ladje[self.ladja].postavljena:
                            break
                    cez_vse += 1  
                self.popravi_postavitev()                 
                self.pokazi_ladjo()
                
    # narise, kje se ladja nahaja
    def pokazi_ladjo(self):
        dolzina = self.igralec.ladje[self.ladja].dolzina
        postavljena = self.igralec.ladje[self.ladja].postavljena
        rotacija = self.igralec.ladje[self.ladja].rotacija
        i = self.vrstica
        j = self.stolpec
        x = j*30 + 2
        y = i*30 + 2
        korak = 30
        if not postavljena:
            for k in range(len(self.igralec.ladje)):
                if rotacija == "ver":
                    if k < dolzina:
                        self.canvas_levi.itemconfig(self.kvadratki[k], state = "normal")
                        self.canvas_levi.coords(self.kvadratki[k], x,y, x+30, y+30)
                        y += korak
                    else:
                       self.canvas_levi.itemconfig(self.kvadratki[k], state = "hidden")

                else:
                    if k < dolzina:
                        self.canvas_levi.itemconfig(self.kvadratki[k], state = "normal")
                        self.canvas_levi.coords(self.kvadratki[k], x,y, x+30, y+30)
                        x += korak
                    else:
                       self.canvas_levi.itemconfig(self.kvadratki[k], state = "hidden")
        self.canvas_levi.focus_set()
        #print("vrstica = {}, stolpec = {}, rotacija = {}, dolzina = {}".format(self.vrstica, self.stolpec, rotacija, dolzina))
    
    # postavijo se ladje za racunalnik in igra se zacne                
    def zacni_igro(self, vse_postavljene = False):
        if self.igralec.je_clovek:
            if not vse_postavljene:
                pass
            else:
                self.zacetek_igre = True
                self.info.set("Ciljaj z z miško.")
                self.navodila.set("")
                for i in range(len(self.kvadratki)):
                    self.canvas_levi.itemconfig(self.kvadratki[i], state = "hidden")
                if not self.racunalnik.je_clovek:
                    self.racunalnik.moje_polje = self.postavi_rac(self.racunalnik.id)
        else:
            self.zacetek_igre = True
            self.info.set("Opazuj dvoboj.")
            self.navodila.set("")
            if not self.racunalnik.je_clovek:
                self.racunalnik.moje_polje = self.postavi_rac(self.racunalnik.id)
                self.igralec.moje_polje = self.postavi_rac(self.igralec.id)
                self.narisi()
    # zacne prvo potezo pri dveh računalnikih        
    def racvsrac(self):
        self.izstreli_rac(self.igralec.id)    
        
    # spremeni kdo je na potezi   
    def na_potezi_je(self):
        if not self.konec:
            if self.na_potezi == 0:
                self.na_potezi = 1
                return 1
            else:
                self.na_potezi = 0
                return 0  
        
    # metoda, ki pobarva kvadratek, ko nanj kliknete z miško
    # uporabljali jo bomo za streljanje 
    def strel(self, event):
        if not self.konec:
            if self.na_potezi == 0:
                if self.zacetek_igre:
                    j = int(event.x // (POLJE_X / X))
                    i = int(event.y // (POLJE_Y / Y))
                    if j > X - 1:
                        j = X-1
                    if i > Y - 1:
                        i = Y - 1 
                    self.izstreli(i,j)
    # ta metoda se poklice, ko je clovek na vrsti in klikne
    # na neko polje    
    def izstreli(self, i , j):
        if self.na_potezi == 0:
            vsebina = self.racunalnik.moje_polje[i][j]
            moja_vsebina = self.igralec.nasp_polje[i][j]
            # ce je v polju za ciljanje zapisana 1 potem smo tja ze streljali
            # in se nic ne zgodi, sicer je tam 0 in lahko streljamo
            if moja_vsebina == 0:
                self.igralec.streli += 1
                if vsebina == 0:
                    self.canvas_desni.itemconfig(self.nasp_kvad[i][j], fill = "gray")
                    self.igralec.nasp_polje[i][j] = -1
                elif vsebina == 1:
                    self.canvas_desni.itemconfig(self.nasp_kvad[i][j], fill = "black")
                    self.igralec.nasp_polje[i][j] = 1
                    self.igralec.zadetki += 1
                    self.igralec.prejsnji = [i, j]
                self.navodila.set("Igralec: ({0} {1}) {2}!".format(IMENA[j], i+1, "Zadeta" if vsebina == 1 else "Zgrešena"))
                self.potopi()
                self.je_konec()
                self.na_potezi_je()
                self.okno.after(50, self.izstreli_rac(self.racunalnik.id))
            
            
    def izstreli_rac(self, id = 1):
        if not self.konec:
            if id == 1:
                zadetek = False
                if self.racunalnik.tezavnost == 0:
                    (i, j) = self.ciljanje.easy(self.racunalnik.nasp_polje)
                elif self.racunalnik.tezavnost == 1:
                    (i, j) = self.ciljanje.normal(self.racunalnik.nasp_polje, self.racunalnik.prejsnji, self.igralec.ladje)
                elif self.racunalnik.tezavnost == 2:
                    (i, j) = self.ciljanje.hard(self.racunalnik.nasp_polje, self.racunalnik.prejsnji, self.igralec.ladje)
                else:
                    (i, j) = self.ciljanje.impossible(self.racunalnik.nasp_polje, self.racunalnik.prejsnji,
                                                       self.igralec.ladje, self.racunalnik.zgreseni, self.racunalnik.streli)
                vsebina = self.igralec.moje_polje[i][j]
                moja_vsebina = self.racunalnik.nasp_polje[i][j]
                if moja_vsebina == 0:
                    self.racunalnik.streli += 1
                    if vsebina == 0:
                        self.canvas_levi.itemconfig(self.moje_kvad[i][j], fill = "gray")
                        self.racunalnik.nasp_polje[i][j] = -1
                        self.racunalnik.zgreseni += 1
                    elif vsebina == 1:
                        self.canvas_levi.itemconfig(self.moje_kvad[i][j], fill = "black")
                        self.racunalnik.nasp_polje[i][j] = 1
                        self.racunalnik.zadetki += 1
                        self.racunalnik.prejsnji = [i, j]
                        self.zgreseni = 0
                    self.navodila.set(self.navodila.get()+" Nasprotnik: ({0} {1}) {2}!".format(IMENA[j], i+1, "Zadeta" if vsebina == 1 else "Zgrešena"))
                    self.potopi()
                    self.je_konec()
                if self.igralec.je_clovek:
                    self.na_potezi_je()
                else:
                    self.okno.update_idletasks()
                    self.okno.after(50)
                    self.izstreli_rac(self.igralec.id)
            else:
                zadetek = False
                if self.igralec.tezavnost == 0:
                    (i, j) = self.ciljanje.easy(self.igralec.nasp_polje)
                elif self.igralec.tezavnost == 1:
                    (i, j) = self.ciljanje.normal(self.igralec.nasp_polje, self.igralec.prejsnji, self.racunalnik.ladje)
                elif self.igralec.tezavnost == 2:
                    (i, j) = self.ciljanje.hard(self.igralec.nasp_polje, self.igralec.prejsnji, self.racunalnik.ladje)
                else:
                    (i, j) = self.ciljanje.impossible(self.igralec.nasp_polje, self.igralec.prejsnji,
                                                       self.racunalnik.ladje, self.igralec.zgreseni, self.igralec.streli)
                vsebina = self.racunalnik.moje_polje[i][j]
                moja_vsebina = self.igralec.nasp_polje[i][j]
                if moja_vsebina == 0:
                    self.igralec.streli += 1
                    if vsebina == 0:
                        self.canvas_desni.itemconfig(self.nasp_kvad[i][j], fill = "gray")
                        self.igralec.nasp_polje[i][j] = -1
                        self.igralec.zgreseni += 1
                    elif vsebina == 1:
                        self.canvas_desni.itemconfig(self.nasp_kvad[i][j], fill = "black")
                        self.igralec.nasp_polje[i][j] = 1
                        self.igralec.zadetki += 1
                        self.igralec.prejsnji = [i, j]
                        self.zgreseni = 0
                    self.navodila.set("Računalnik 1: ({0} {1}) {2}!".format(IMENA[j], i+1, "Zadeta" if vsebina == 1 else "Zgrešena"))
                    self.potopi()
                    self.je_konec()
                    self.okno.update_idletasks()
                    self.okno.after(50)
                    self.izstreli_rac(self.racunalnik.id)
    #za vse ladje, preveri, ce so potopljene   
    def potopi(self):
        if not self.konec:
            if self.igralec.potopljene !=[self.racunalnik.ladje[k].je_potopljena(self.igralec.nasp_polje) for k in range(len(LADJE))]:
                self.igralec.potopljene = [self.racunalnik.ladje[k].je_potopljena(self.igralec.nasp_polje) for k in range(len(LADJE))]
                self.igralec.prejsnji = None
            if self.racunalnik.potopljene !=[self.igralec.ladje[k].je_potopljena(self.racunalnik.nasp_polje) for k in range(len(LADJE))]:
                self.racunalnik.potopljene =[self.igralec.ladje[k].je_potopljena(self.racunalnik.nasp_polje) for k in range(len(LADJE))]
                self.racunalnik.prejsnji = None
            for i in range(len(LADJE)):
                self.igralec.ladje[i].pobarvaj_potopljene(self.canvas_levi, self.moje_kvad)
                self.racunalnik.ladje[i].pobarvaj_potopljene(self.canvas_desni, self.nasp_kvad)
        
    # preveri ce je igre ze konec
    def je_konec(self):
        zmaga_clovek = False
        zmaga_racunalnik = False
        if False not in self.racunalnik.potopljene:
                zmaga_racunalnik = True
        if False not in self.igralec.potopljene:
                zmaga_clovek = True
        if zmaga_clovek:
            self.info.set("Zmagal si! Stevilo potez: {}".format(self.igralec.streli))
        if zmaga_racunalnik:
            self.info.set("Zmagal je racunalnik Stevilo potez: {}".format(self.racunalnik.streli))
        if True in (zmaga_clovek, zmaga_racunalnik):
            natancnost1 = 100* self.igralec.zadetki / self.igralec.streli
            natancnost2 = 100 * self.racunalnik.zadetki / self.racunalnik.streli
            self.navodila.set("Tvoja natančnost: {0:3.1f}%. Računalnikova natančnost: {1:3.1f}%.".format(natancnost1, natancnost2)) 
            self.konec = True
            
    
    # koncaj igro in začni novo
    def nova(self):
        for i in range(1): 
            self.canvas_bot[i].grid_forget()
        self.canvas_mid.grid_forget()
        self.gumb_nova.grid_forget()
        self.canvas_levi.grid_forget()
        self.canvas_desni.grid_forget()
        zacni_novo(True, root = self.okno)
        return
    
        
class Glavni_meni():
    
    # to je kar se pojavi na zacetku, kjer izberemo tezavnost in nastavitve
    def __init__(self, master):
        self.max_tezavnost = 3
        self.min_tezavnost = 0
        self.tezavnost0 = 0
        self.tezavnost1 = 0
        self.igralec = True
        self.racunalnik = False
        self.okno = master
        self.igralec_str ="Igralec" if self.igralec else "Računalnik" 
        self.racunalnik_str = "igralcu" if self.racunalnik else "računalniku"
        self.okno.columnconfigure(1, minsize = 220)
        
        
        #napis, ki prikazuje trenutne nastavitve
        self.nastavitve = StringVar(value = "{0} proti {1}".format(self.igralec_str, self.racunalnik_str))
        self.label_nastavitve = Label(master, textvariable=self.nastavitve)
        self.label_nastavitve.grid(row=0, column=1)
        
        self.nastavitve1 = StringVar(value = "Težavnost: {0}".format(self.tez1()))
        self.label_nastavitve1 = Label(master, textvariable=self.nastavitve1)
        self.label_nastavitve1.grid(row=1, column=1)
        
        
        # gumb, ki omogoca zacetek nove igre
        self.gumb_zacni = Button(master, text="Začni novo igro", command=self.zacni)
        self.gumb_zacni.grid(row = 0, column = 0)
        
        #gumb, ki doloci ali je standardna igra clovek proti racunalniku
        self.gumb_standard = Button(master, text="Človek vs Računalnik", command=self.standard)
        self.gumb_standard.grid(row = 2, column = 0)
        
        #gumb, ki doloci ali je igra racunalnik proti racunalniku
        self.gumb_r_v_r = Button(master, text="Računalnik vs Računalnik", command=self.racunalnik_v_racunalnik)
        self.gumb_r_v_r.grid(row = 2, column = 1)
        
        #gumb, ki povecuje tezavnost racunalnika z id 0
        self.gumb_tezje1 = Button(master, text="Povecaj tezavnost", command=self.tezje1)
        self.gumb_tezje1.grid(row = 3, column = 1)
        
        #gumb, ki zmanjsuje tezavnosti racunalnika z id 0
        self.gumb_lazje1 = Button(master, text="Zmanjsaj tezavnost", command=self.lazje1)
        self.gumb_lazje1.grid(row = 3, column = 0)
        
        #gumb, ki povecuje tezavnost racunalnika z id 1
        self.gumb_tezje0 = Button(master, text="Povecaj tezavnost 2", command=self.tezje0, state = "disabled")
        self.gumb_tezje0.grid(row = 4, column = 1)
        
        #gumb, ki zmanjsuje tezavnosti racunalnika z id 1
        self.gumb_lazje0 = Button(master, text="Zmanjsaj tezavnost 2", command=self.lazje0, state = "disabled")
        self.gumb_lazje0.grid(row = 4, column = 0)
        
        
    def zacni(self):
        self.label_nastavitve.grid_forget()
        self.gumb_zacni.grid_forget()
        self.gumb_standard.grid_forget()
        self.gumb_r_v_r.grid_forget()
        self.gumb_tezje0.grid_forget()
        self.gumb_lazje0.grid_forget()
        self.gumb_tezje1.grid_forget()
        self.gumb_lazje1.grid_forget()
        self.label_nastavitve1.grid_forget()
        self.okno.columnconfigure(1, minsize = 0)
        self.game = Game(self.okno, self.tezavnost0, self.tezavnost1, self.igralec, self.racunalnik)
    
    def standard(self):
        self.igralec = True
        self.racunalnik = False
        self.igralec_str = "Igralec"
        self.racunalnik_str = "računalniku"
        self.gumb_tezje0.config(state = "disabled")
        self.gumb_lazje0.config(state = "disabled")
        self.nastavitve.set("{0} proti {1}".format(self.igralec_str, self.racunalnik_str))
        self.nastavitve1.set("Težavnost: {0}".format(self.tez1()))
    def racunalnik_v_racunalnik(self):
        self.igralec = False
        self.racunalnik = False
        self.igralec_str = "Računalnik"
        self.racunalnik_str = "računalniku"
        self.gumb_tezje0.config(state = "normal")
        self.gumb_lazje0.config(state = "normal")
        self.nastavitve.set("{0} proti {1}".format(self.igralec_str, self.racunalnik_str))
        self.nastavitve1.set("Težavnost 1: {0}, Težavnost 2: {1}".format(self.tez1(), self.tez0()))
    def tezje0(self):
        if self.tezavnost0 + 1 > self.max_tezavnost:
            return
        else:
            self.tezavnost0 += 1
            self.napisi()
        
    def lazje0(self):
        if self.tezavnost0 - 1 < self.min_tezavnost:
            return
        else:
            self.tezavnost0 -= 1
            self.napisi()
            
    def tezje1(self):
        if self.tezavnost1 + 1 > self.max_tezavnost:
            return
        else:
            self.tezavnost1 += 1
            self.napisi()
        
    
    def lazje1(self):
        if self.tezavnost1 - 1 < self.min_tezavnost:
            return
        else:
            self.tezavnost1 -= 1
            self.napisi()
        
    def tez0(self):
        if self.tezavnost0 == 0:
            return "lahka"
        elif self.tezavnost0 == 1:
            return "srednja"
        elif self.tezavnost0 == 2:
            return "težka"
        else:
            return "najtežja"
    def tez1(self):
        if self.tezavnost1 == 0:
            return "lahka"
        elif self.tezavnost1 == 1:
            return "srednja"
        elif self.tezavnost1 == 2:
            return "težka"
        else:
            return "najtežja"
    def napisi(self):
        if self.igralec:
            self.nastavitve.set("{0} proti {1}".format(self.igralec_str, self.racunalnik_str))
            self.nastavitve1.set("Težavnost: {0}".format(self.tez1()))
        else:
            self.nastavitve.set("{0} proti {1}".format(self.igralec_str, self.racunalnik_str))
            self.nastavitve1.set("Težavnost 1: {0}, Težavnost 2: {1}".format(self.tez1(), self.tez0()))
        
        
        

def zacni_novo(poteka, root = None):
    if poteka:
        root.destroy()
    root = Tk()
    root.title("Battleships")  
    igra = Glavni_meni(root)
    root.mainloop()

zacni_novo(False)
    
    