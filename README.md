Avtor: Urban Strajnar
Repo: https://bitbucket.org/urbanstrajnar/battleships/

=== Opis igrice ===

Igrica [Potapljanje ladij](http://en.wikipedia.org/wiki/Battleship_game). Gre za igro pri kateri dva igralca ugibata, kam je drugi postavil svoje ladje in jih skuša potopiti. Zmaga tisti, ki prvi potopi vse nasprotnikove ladje.


== Podrobnejši opis ==

Igralno polje bo velikosti 10 x 10. Vsak igralec bo imel 5 ladij, eno dolžine 2, dve dolžine 3, eno dolžine 4 in eno dolžine 5. Pred začetkom igre vsak igralec postavi vse svoje ladje, kamorkoli si želi. Pravilo pri postavljanju ladij je, da se ne smejo dotikati. Lahko se dotikajo samo diagonalno. ![img5005.gif](https://bitbucket.org/repo/zKbAB7/images/922254544-img5005.gif)
Slika 1: Ena možna začetna postavitev

Stolpci so označeni s številkami od 1 do 10, vrstice pa s črkami od A do J. Vsak igralec vidi svoje polja in zakrito (prazno) nasprotnikovo polje.
Igralca izmenjaje imenujeta polja na nasprotnikovem polju, kamor "izstrelita bombo". Igralec lahko zgreši, v tem primeru se nariše na imenovano polje znak za zgrešen strel. Lahko pa zadane, v tem primeru, se pa na polje nariše znak za zadetek in igralec, ki je zadel, je še enkrat na vrsti.

=== Potek dela ===
1) S  pomočjo tkinterja bom narisal igralno polje.

2) Napisal bom kodo, ki bo omogočala postavitev začetnega polja za človeka in za računalnik.

3) Napisal bom "game logic", ki bo omogočala streljanje, označevala zadetke in zgrešene strele, ponavljanje strelov ob primeru zadetka ipd.

4) Naredil bom neinteligenten AI, ki bo naključno izbiral polja zato, da bom imel nasprotnika za reševanje bugov v igri.

5)Napisal bom malo pametnejši AI, si bo zapomnil, kdaj je zadel in naslednje strele izbiral temu primerno. Ta AI bo že uporabljal hevristiko in bo za strel v bližino prejšnjega zadetka dobil visoko oceno.

6)Napisal bom AI, ki bo (upam) z lahkoto premagal človeškega igralca, tako, da bo za vsako polje izračunal verjetnost, da se nasprotnikova ladja tam nahaja.

''Kar je tu spodaj, bo implementirano samo, če bo za to dovolj časa. Vrstni red zapisa alinej ni pomemben.''


- Animacija strelov, zgrešenih strelov in zadetkov. Odvisno od nadležnosti.

-Predvajanje glasbe in zvokov. Odvisno od nadležnosti.

-Možna izbira izstrelkov recimo: 1x3, 2x2, diagonalno ipd. Teh izstrelkov bo omejeno število.

-Računalnik, ki bo igral proti drugemu računalniku. V tem primeru bosta prikazani igralni polji obeh igralcev.


-V kodi bom implementiral metodo, ki si bo po končani igri zapomnil igralčevo postavitev ladij. Igralec bo pred začetkom zapisal svoje ime, igra bo ime zapisala v datoteko ter zraven dodala vse igralčeve prejšnje postavitve oz. njegov način igranja. To bo pri igranju z njim upoštevala. Možno bo izbrati igro v tem načinu ali v anonimnem, kjer računalnik ne bo nič vedel o prejšnjih igrah.